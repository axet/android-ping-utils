package com.github.axet.pingutils.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.github.axet.androidlibrary.activities.AppCompatThemeActivity;
import com.github.axet.androidlibrary.app.MainApplication;
import com.github.axet.androidlibrary.widgets.DotsTabView;
import com.github.axet.androidlibrary.widgets.OpenFileDialog;
import com.github.axet.pingutils.R;
import com.github.axet.pingutils.fragments.AboutFragment;
import com.github.axet.pingutils.fragments.DigFragment;
import com.github.axet.pingutils.fragments.InfoFragment;
import com.github.axet.pingutils.fragments.MonitorFragment;
import com.github.axet.pingutils.fragments.PingFragment;
import com.github.axet.pingutils.fragments.RouteFragment;
import com.github.axet.pingutils.widgets.StatusTabView;

public class MainActivity extends AppCompatThemeActivity {
    public static int STATUS_TAB_INDEX = 0;

    public TabLayout tabs;

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final Context mContext;

        public SectionsPagerAdapter(Context context, FragmentManager fm) {
            super(fm);
            mContext = context;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return MonitorFragment.newInstance();
                case 1:
                    return InfoFragment.newInstance();
                case 2:
                    return DigFragment.newInstance();
                case 3:
                    return RouteFragment.newInstance();
                case 4:
                    return PingFragment.newInstance();
                case 5:
                    return AboutFragment.newInstance();
            }
            return null;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return StatusTabView.DOTS;
                case 1:
                    return "INFO";
                case 2:
                    return "DIG";
                case 3:
                    return "ROUTE";
                case 4:
                    return "PING";
                case 5:
                    return DotsTabView.DOTS;
            }
            return "";
        }

        @Override
        public int getCount() {
            return 6;
        }
    }

    @Override
    public int getAppTheme() {
        return MainApplication.getTheme(this, R.style.Theme_PingUtilsLight_NoActionBar, R.style.Theme_PingUtilsDark_NoActionBar, R.style.Theme_PingUtilsDarkBlack_NoActionBar);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        tabs = (TabLayout) findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(1);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE)
                    OpenFileDialog.hideKeyboard(MainActivity.this, viewPager);
            }
        });
        StatusTabView.update(tabs, 0);
        DotsTabView.update(tabs, 5);
    }
}