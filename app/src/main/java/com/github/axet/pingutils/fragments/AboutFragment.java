package com.github.axet.pingutils.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.axet.androidlibrary.preferences.AboutPreferenceCompat;
import com.github.axet.pingutils.R;

public class AboutFragment extends Fragment {
    public static String TAG = AboutFragment.class.getSimpleName();

    public static AboutFragment newInstance() {
        AboutFragment fragment = new AboutFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return AboutPreferenceCompat.buildView(getContext(), AboutPreferenceCompat.replaceVersion(getContext(), AboutPreferenceCompat.loadStringResource(getContext(), R.raw.about)));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}