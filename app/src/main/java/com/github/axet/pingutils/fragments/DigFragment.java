package com.github.axet.pingutils.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.github.axet.androidlibrary.widgets.ErrorDialog;
import com.github.axet.androidlibrary.widgets.OpenFileDialog;
import com.github.axet.pingutils.R;
import com.github.axet.pingutils.app.DnsServersDetector;
import com.github.axet.pingutils.widgets.InstantAutoComplete;

import org.xbill.DNS.DClass;
import org.xbill.DNS.Message;
import org.xbill.DNS.Name;
import org.xbill.DNS.Record;
import org.xbill.DNS.ReverseMap;
import org.xbill.DNS.Section;
import org.xbill.DNS.SimpleResolver;
import org.xbill.DNS.Type;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Date;

public class DigFragment extends Fragment {
    public static String TAG = DigFragment.class.getSimpleName();

    private static final String ARG_SECTION_NUMBER = "section_number";

    Handler handler = new Handler();
    Thread thread;
    Snackbar sb;

    public static DigFragment newInstance() {
        DigFragment fragment = new DigFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, 1);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static String reverseLookupDns(String server, String h) throws IOException {
        Name name = ReverseMap.fromAddress(h);
        SimpleResolver res;
        if (server == null || server.isEmpty()) {
            res = new SimpleResolver(new InetSocketAddress("127.0.0.1", SimpleResolver.DEFAULT_PORT));
        } else {
            res = new SimpleResolver(server);
        }
        Record rec = Record.newRecord(name, Type.PTR, DClass.IN);
        Message query = Message.newQuery(rec);
        Message response = res.send(query);
        final Record[] answers = response.getSectionArray(Section.ANSWER);
        if (answers.length > 0) {
            String ret = answers[0].rdataToString();
            if (ret.endsWith(".")) {
                ret = ret.substring(0, ret.length() - 1);
                return ret;
            }
        }
        return null;
    }

    public static String dig(String server, boolean ipv6, String n) throws IOException {
        StringBuilder out = new StringBuilder();
        Name name = Name.fromString(n, Name.root);
        int type = ipv6 ? Type.AAAA : Type.A;
        int dclass = DClass.IN;
        SimpleResolver res;
        if (server == null || server.isEmpty()) {
            res = new SimpleResolver(new InetSocketAddress("127.0.0.1", SimpleResolver.DEFAULT_PORT));
        } else {
            res = new SimpleResolver(server);
        }
        Record rec = Record.newRecord(name, type, dclass);
        Message query = Message.newQuery(rec);
        long startTime = System.currentTimeMillis();
        Message response = res.send(query);
        long endTime = System.currentTimeMillis();
        long ms = endTime - startTime;
        out.append("; dnsjava dig");
        out.append("\n");
        out.append(response);
        out.append("\n");
        out.append(";; Query time: " + ms + " ms");
        out.append("\n");
        InetSocketAddress addr = res.getAddress();
        out.append(String.format(";; SERVER: %s#%d(%s) (UDP)", addr.getAddress().getHostAddress(), addr.getPort(), server));
        out.append("\n");
        out.append(String.format(";; WHEN: %s", new Date(startTime)));
        out.append("\n");
        return out.toString();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_dig, container, false);

        DnsServersDetector dns = new DnsServersDetector(getContext());
        DnsServersDetector.Pair[] ss = dns.getServers();
        String[] kk = DnsServersDetector.toNames(ss);
        ArrayAdapter<String> dd = new ArrayAdapter<>(getContext(), android.R.layout.simple_dropdown_item_1line, kk);
        InstantAutoComplete edit = (InstantAutoComplete) root.findViewById(R.id.dns_edit);
        edit.setAdapter(dd);
        if (edit.getText().toString().isEmpty())
            edit.setText(dd.getItem(0));

        FloatingActionButton fab = (FloatingActionButton) root.findViewById(R.id.fab);

        EditText pingEdit = (EditText) root.findViewById(R.id.ping_edit);
        TextView output = (TextView) root.findViewById(R.id.output);
        CheckBox ipv6 = (CheckBox) root.findViewById(R.id.ipv6);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenFileDialog.hideKeyboard(getContext(), edit);
                OpenFileDialog.hideKeyboard(getContext(), pingEdit);
                String name = pingEdit.getText().toString();
                if (thread != null)
                    thread.interrupt();
                thread = new Thread() {
                    @Override
                    public void run() {
                        StringBuilder out = new StringBuilder();
                        Runnable run = new Runnable() {
                            @Override
                            public void run() {
                                output.setText(out.toString());
                                sb.dismiss();
                            }
                        };
                        try {
                            out.append(dig(edit.getText().toString().trim(), ipv6.isChecked(), name));
                        } catch (IOException e) {
                            Log.e(TAG, "Error", e);
                            if (Thread.currentThread().isInterrupted())
                                return;
                            out.append(ErrorDialog.toMessage(e));
                        }
                        handler.post(run);
                    }
                };
                if (sb != null)
                    sb.dismiss();
                sb = Snackbar.make(view, "Quering", Snackbar.LENGTH_LONG)
                        .setAction("Action", null)
                        .setDuration(Snackbar.LENGTH_INDEFINITE);
                sb.show();
                thread.start();
            }
        });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}