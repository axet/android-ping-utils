package com.github.axet.pingutils.fragments;

import android.Manifest;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.RouteInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.axet.androidlibrary.app.AssetsDexLoader;
import com.github.axet.androidlibrary.app.Storage;
import com.github.axet.pingutils.R;
import com.github.axet.pingutils.app.DnsServersDetector;
import com.github.axet.pingutils.app.WifiInfo;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

public class InfoFragment extends Fragment {
    public static String TAG = InfoFragment.class.getSimpleName();

    private static final String ARG_SECTION_NUMBER = "section_number";
    public static final String NL = "<br/>";
    public static final String TAB = "\t";
    public static String[] PERMISSIONS = new String[]{"android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION"};
    public static String[] PERMISSIONS2 = new String[]{Manifest.permission.READ_PHONE_STATE};
    public static int RESULT = 1;

    Runnable refresh;

    int asu;
    int signal = Integer.MIN_VALUE;

    PhoneStateListener l = new PhoneStateListener() {
        @Override
        public void onSignalStrengthChanged(int a) {
            asu = a;
        }

        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            int ss = signalStrength.getGsmSignalStrength(); // valid range 0-31 or 99
            if (ss >= 0 && ss <= 31)
                signal = 2 * ss - 113;
            else
                signal = Integer.MIN_VALUE;
        }
    };

    public static boolean startsWith(byte[] bb, byte[] b2) {
        byte[] first = Arrays.copyOfRange(bb, 0, b2.length);
        return Arrays.equals(first, b2);
    }

    public static boolean isIPPrivate(InetAddress o1) {
        if (o1 instanceof Inet6Address) {
            if (startsWith(o1.getAddress(), new byte[]{(byte) 0xfc, 0})) // java missing 0xfc00 private range
                return true;
        }
        return o1.isLinkLocalAddress() || o1.isLoopbackAddress() || o1.isSiteLocalAddress();
    }

    public static String getNetworkClass(int t) {
        switch (t) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return "2G";
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return "3G";
            case TelephonyManager.NETWORK_TYPE_LTE:
                return "4G";
//            case TelephonyManager.NETWORK_TYPE_NR:
//                return "5G";
            default:
                return null;
        }
    }

    public static String getNetworkName(int t) {
        switch (t) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
                return "GPRS";
            case TelephonyManager.NETWORK_TYPE_EDGE:
                return "EDGE";
            case TelephonyManager.NETWORK_TYPE_CDMA:
                return "CDMA";
            case TelephonyManager.NETWORK_TYPE_1xRTT:
                return "1xRTT";
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return "IDEN";
            case TelephonyManager.NETWORK_TYPE_UMTS:
                return "UMTS";
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
                return "EVDO_0";
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                return "EVDO_A";
            case TelephonyManager.NETWORK_TYPE_HSDPA:
                return "HSDPA";
            case TelephonyManager.NETWORK_TYPE_HSUPA:
                return "HSUPA";
            case TelephonyManager.NETWORK_TYPE_HSPA:
                return "HSPA";
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
                return "EVDO_B";
            case TelephonyManager.NETWORK_TYPE_EHRPD:
                return "EHRPD";
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return "HSPAP";
            case TelephonyManager.NETWORK_TYPE_LTE:
                return "LTE";
        }
        return null;
    }

    public static InfoFragment newInstance() {
        InfoFragment fragment = new InfoFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, 1);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static boolean isOnline(Context context, Network net) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (net == null)
                return false;
            NetworkCapabilities cap = cm.getNetworkCapabilities(net);
            if (cap == null)
                return false;
            return cap.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) && cap.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED);
        } catch (Exception e) {
            Log.w(TAG, e);
        }
        return false;
    }

    public static String formatInterface(InterfaceAddress ia) {
        String str = "";
        InetAddress a = ia.getAddress();
        if (a instanceof Inet4Address)
            str += "inet4 ";
        if (a instanceof Inet6Address)
            str += "inet6 ";
        try {
            str += InetAddress.getByAddress(a.getAddress()).getHostAddress();
        } catch (UnknownHostException e) {
            Log.w(TAG, e);
        }
        str += "/";
        str += ia.getNetworkPrefixLength();
        return str;
    }

    public static StringBuilder formatDns(Context context) {
        final StringBuilder tt = new StringBuilder();
        DnsServersDetector dns = new DnsServersDetector(context);
        DnsServersDetector.Pair[] ss = dns.getServersMethodSystemProperties();
        if (ss != null && ss.length > 0) {
            tt.append("<h2>DNS (System props):</h2>"); // net.dns1, net.dns2, net.dns3, net.dns4
            for (DnsServersDetector.Pair a : ss) {
                tt.append("<b>" + a.name + ":</b> " + a.value);
                tt.append(NL);
            }
        }
        ss = dns.getServersMethodConnectivityManager();
        if (ss != null && ss.length > 0) {
            if (tt.length() > 0)
                tt.append(NL);
            tt.append("<h2>DNS:</h2>");
            for (DnsServersDetector.Pair a : ss) {
                tt.append("<b>" + a.name + ":</b> " + a.value);
                tt.append(NL);
            }
        }
        ss = dns.getServersMethodExec();
        if (ss != null && ss.length > 0) {
            if (tt.length() > 0)
                tt.append(NL);
            tt.append("<h2>getprop with *.dns1:</h2>");
            for (DnsServersDetector.Pair a : ss) {
                tt.append(a);
                tt.append(NL);
            }
        }
        return tt;
    }

    public static StringBuilder formatNet(TreeSet<String> dd) {
        StringBuilder tt = new StringBuilder();
        tt.append("<h2>Network Interfaces:</h2>");
        try {
            ArrayList<NetworkInterface> nn = Collections.list(NetworkInterface.getNetworkInterfaces());
            Collections.sort(nn, new NetworkInteracesSort(dd));
            for (NetworkInterface ni : nn) {
                String str0 = "";
                String str = "";
                str0 += "<b>" + ni.getName() + ":" + "</b>";
                str0 += NL;
                byte[] bb = ni.getHardwareAddress();
                if (bb != null) {
                    str += TAB + "mac ";
                    for (byte b : bb)
                        str += String.format("%02X:", b);
                    str = str.substring(0, str.length() - 1);
                    str += NL;
                }
                List<InterfaceAddress> aa = ni.getInterfaceAddresses();
                Collections.sort(aa, new SortIP4First());
                for (InterfaceAddress ia : aa) {
                    str += TAB;
                    str += formatInterface(ia);
                    str += NL;
                }
                if (!str.isEmpty()) {
                    tt.append(str0);
                    tt.append(str);
                    tt.append(NL);
                }
            }
        } catch (Exception e) {
            Log.w(TAG, e);
        }
        return tt;
    }

    public static StringBuilder formatRoutes(Context context, TreeSet<String> dd) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        LinkProperties link = cm.getLinkProperties(cm.getActiveNetwork());
        if (link == null)
            return null;
        StringBuilder tt = new StringBuilder();
        tt.append("<h2>Routes:</h2>");
        List<RouteInfo> rr = new ArrayList<>(link.getRoutes());
        Collections.sort(rr, new SortIP4FirstDefRoute());
        for (RouteInfo info : rr) {
            tt.append(TAB);
            if (info.isDefaultRoute())
                tt.append("<b>");
            tt.append(info.getDestination());
            tt.append(" ");
            try {
                InetAddress gw = info.getGateway();
                tt.append(" via ");
                tt.append(InetAddress.getByAddress(gw.getAddress()).getHostAddress());
                tt.append(" ");
            } catch (Exception e) {
                Log.w(TAG, e);
            }
            tt.append(" dev ");
            String it = info.getInterface();
            tt.append(it);
            if (info.isDefaultRoute()) {
                tt.append("</b>");
                dd.add(it);
            }
            tt.append(NL);
        }
        return tt;
    }

    public static StringBuilder formatOnline(Context context, Network net) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        StringBuilder tt = new StringBuilder();
        NetworkInfo info = cm.getNetworkInfo(net);
        NetworkInfo.DetailedState st = info.getDetailedState();
        tt.append("<b>").append("Status: ").append("</b>").append(st);
        if (st == NetworkInfo.DetailedState.CONNECTED && !isOnline(context, net))
            tt.append(" (No internet)");
        tt.append(NL);
        return tt;
    }

    public static String formatKbps(long size) {
        if (size <= 0) return "0";
        final String[] units = new String[]{"B", "Kb", "Mb", "Gb", "Tb", "Pb", "Eb"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1000));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1000, digitGroups)) + " " + units[digitGroups] + "/s";
    }

    public static StringBuilder formatSpeed(Context context, Network net) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkCapabilities cap = cm.getNetworkCapabilities(net);
        if (cap == null)
            return null;
        StringBuilder tt = new StringBuilder();
        int d = cap.getLinkDownstreamBandwidthKbps() * 1000;
        int u = cap.getLinkUpstreamBandwidthKbps() * 1000;
        tt.append("<b>").append("Speed: ").append("</b>").append(formatKbps(d)).append(" (down) / ").append(formatKbps(u)).append(" (up)").append(NL);
        return tt;
    }

    public static StringBuilder formatNet(Context context, String name, Network net) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getNetworkInfo(net);
        if (info == null)
            return null;
        StringBuilder tt = new StringBuilder();
        tt.append("<h2>").append(name).append("</h2>");
        String s = info.getExtraInfo();
        if (s != null && !s.isEmpty())
            tt.append("<b>").append("mac: ").append("</b>").append(Html.escapeHtml(s)).append(NL);
        try {
            tt.append(formatOnline(context, net));
        } catch (Exception e) {
            Log.w(TAG, e);
        }
        StringBuilder b = formatSpeed(context, net);
        if (b != null)
            tt.append(b);
        tt.append(NL);
        return tt;
    }

    public static StringBuilder formatWifi(Fragment fragment, WifiInfo wifi) {
        if (!Storage.permitted(fragment, PERMISSIONS, RESULT))
            return null;
        StringBuilder tt = new StringBuilder();
        android.net.wifi.WifiInfo winfo = wifi.winfo;
        if (winfo != null) {
            tt.append("<h2>Wifi");
            if (wifi.band5ghz || wifi.band24ghz) {
                tt.append(" (");
                if (wifi.band24ghz)
                    tt.append("2.4GHz");
                if (wifi.band5ghz) {
                    if (wifi.band24ghz)
                        tt.append("/");
                    tt.append("5GHz");
                }
                tt.append(")");
            }
            tt.append("</h2>");
            String s = winfo.getSSID();
            if (s != null && !s.isEmpty())
                tt.append("<b>").append("SSID: ").append("</b>").append(Html.escapeHtml(s)).append(NL);
            s = winfo.getBSSID();
            if (s != null && !s.isEmpty())
                tt.append("<b>").append("BSSID: ").append("</b>").append(s).append(NL);
            int f = winfo.getFrequency();
            if (f > 0) {
                tt.append("<b>").append("Freq: ").append("</b>").append(f).append("MHz").append(NL);
                tt.append("<b>").append("Channel: ").append("</b>").append(WifiInfo.convertFrequencyToChannel(f)).append(NL);
            }
            try {
                tt.append(formatOnline(fragment.getContext(), wifi.net));
            } catch (Exception e) {
                Log.w(TAG, e);
            }
            StringBuilder b = formatSpeed(fragment.getContext(), wifi.net);
            if (b != null)
                tt.append(b);
            tt.append(NL);
            return tt;
        }
        return null;
    }

    public static StringBuilder formatMobile(Context context, int signal, int asu, Network net) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (net == null)
            return null;
        NetworkInfo info = cm.getNetworkInfo(net);
        if (info == null)
            return null;
        if (!Storage.permitted(context, PERMISSIONS2))
            return null;
        int networkType = tm.getNetworkType();
        if (networkType == 0)
            return null;
        StringBuilder tt = new StringBuilder();
        tt.append("<h2>Mobile");
        String s = getNetworkClass(networkType);
        if (s != null)
            tt.append(" (").append(s).append(")");
        tt.append("</h2>");
        s = tm.getSimOperatorName();
        if (s != null && !s.isEmpty())
            tt.append("<b>").append("Operator: ").append("</b>").append(Html.escapeHtml(s)).append(NL);

        if (Build.VERSION.SDK_INT >= 28) {
            int m = signal;
            try {
                SignalStrength ss = (SignalStrength) AssetsDexLoader.getPrivateMethod(TelephonyManager.class, "getSignalStrength").invoke(tm);
                m = ss.getLevel();
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                Log.d(TAG, "error", e);
            }
            if (m != 0) {
                tt.append("<b>").append("Signal: ").append("</b>").append(m);
                tt.append(" dBm");
                if (asu != 0)
                    tt.append(", asu: ").append(asu);
                tt.append(NL);
            }
        } else if (Build.VERSION.SDK_INT >= 17) {
            for (CellInfo cinfo : tm.getAllCellInfo()) {
                if (!cinfo.isRegistered())
                    continue;
                int ss = 0;
                int aa = 0;
                if (cinfo instanceof CellInfoGsm) {
                    CellInfoGsm gsm = ((CellInfoGsm) cinfo);
                    ss = gsm.getCellSignalStrength().getDbm();
                    aa = gsm.getCellSignalStrength().getAsuLevel();
                }
                if (cinfo instanceof CellInfoCdma) {
                    CellInfoCdma lte = ((CellInfoCdma) cinfo);
                    ss = lte.getCellSignalStrength().getDbm();
                    aa = lte.getCellSignalStrength().getAsuLevel();
                }
                if (cinfo instanceof CellInfoWcdma) {
                    CellInfoWcdma lte = ((CellInfoWcdma) cinfo);
                    ss = lte.getCellSignalStrength().getDbm();
                    aa = lte.getCellSignalStrength().getAsuLevel();
                }
                if (cinfo instanceof CellInfoLte) {
                    CellInfoLte lte = ((CellInfoLte) cinfo);
                    ss = lte.getCellSignalStrength().getDbm();
                    aa = lte.getCellSignalStrength().getAsuLevel();
                }
                if (ss == 0 || ss == Integer.MAX_VALUE)
                    continue;
                tt.append("<b>").append("Signal: ").append("</b>").append(ss);
                tt.append(" dBm");
                if (aa != 0)
                    tt.append(", asu: ").append(aa);
                tt.append(NL);
            }
        } else {
            if (signal != 0 && signal != Integer.MAX_VALUE) {
                tt.append("<b>").append("Signal: ").append("</b>").append(signal);
                tt.append(" dBm");
                if (asu != 0)
                    tt.append(", asu: ").append(asu);
                tt.append(NL);
            }
        }

        s = getNetworkName(networkType);
        if (s != null && !s.isEmpty()) {
            tt.append("<b>").append("Network: ").append("</b>").append(Html.escapeHtml(s));
            tt.append(NL);
        }
        try {
            tt.append(formatOnline(context, net));
        } catch (Exception e) {
            Log.w(TAG, e);
        }
        for (CellInfo ci : tm.getAllCellInfo()) { // TODO add band calculator
            if (!ci.isRegistered())
                continue;
            if (ci instanceof CellInfoGsm)
                tt.append("<b>").append("Arfcn: ").append("</b>").append(((CellInfoGsm) ci).getCellIdentity().getArfcn()).append(NL);
            else if (ci instanceof CellInfoWcdma)
                tt.append("<b>").append("Uarfcn: ").append("</b>").append(((CellInfoWcdma) ci).getCellIdentity().getUarfcn()).append(NL);
            else if (ci instanceof CellInfoLte)
                tt.append("<b>").append("Earfcn: ").append("</b>").append(((CellInfoLte) ci).getCellIdentity().getEarfcn()).append(NL);
        }
        StringBuilder b = formatSpeed(context, net);
        if (b != null)
            tt.append(b);
        tt.append(NL);
        return tt;
    }

    public static class NetworkInteracesSort implements Comparator<NetworkInterface> {
        TreeSet<String> dd;

        public static boolean hasIP(NetworkInterface i) {
            return !i.getInterfaceAddresses().isEmpty();
        }

        public static boolean isLoopback(NetworkInterface i) {
            try {
                return i.isLoopback();
            } catch (IOException ignore) {
                return i.getName().equals("lo");
            }
        }

        public NetworkInteracesSort(TreeSet<String> dd) {
            this.dd = dd;
        }

        public boolean isDefault(NetworkInterface i) {
            return dd.contains(i.getName());
        }

        @Override
        public int compare(NetworkInterface o1, NetworkInterface o2) {
            if (isDefault(o1) && !isDefault(o2))
                return -1;
            if (!isDefault(o1) && isDefault(o2))
                return 1;
            if (isLoopback(o1) && !isLoopback(o2))
                return -1;
            if (!isLoopback(o1) && isLoopback(o2))
                return 1;
            if (hasIP(o1) && !hasIP(o2))
                return -1;
            if (hasIP(o2) && !hasIP(o1))
                return 1;
            return 0;
        }
    }

    public static class SortIP4First implements Comparator<InterfaceAddress> {
        public static int compareIp4Ip6(InetAddress o1, InetAddress o2) { // sort ip4 first, ip6 last
            return Integer.compare(o1.getAddress().length, o2.getAddress().length);
        }

        public static int comparePrivate(InetAddress o1, InetAddress o2) { // sort private first, public last
            return Boolean.compare(!isIPPrivate(o1), !isIPPrivate(o2));
        }

        @Override
        public int compare(InterfaceAddress o1, InterfaceAddress o2) {
            int i = compareIp4Ip6(o1.getAddress(), o2.getAddress());
            if (i != 0)
                return i;
            return comparePrivate(o1.getAddress(), o2.getAddress());
        }
    }

    public static class NetworkActiveSort implements Comparator<Network> {
        Network net;
        ConnectivityManager cm;

        public NetworkActiveSort(ConnectivityManager cm) {
            this.cm = cm;
            this.net = cm.getActiveNetwork();
        }

        @Override
        public int compare(Network o1, Network o2) {
            if (o1.equals(net))
                return -1;
            if (o2.equals(net))
                return 1;
            return Integer.compare(cm.getNetworkInfo(o1).getType(), cm.getNetworkInfo(o2).getType());
        }
    }

    public static class SortIP4FirstDefRoute implements Comparator<RouteInfo> {
        @Override
        public int compare(RouteInfo o1, RouteInfo o2) {
            int i = SortIP4First.compareIp4Ip6(o1.getDestination().getAddress(), o2.getDestination().getAddress());
            if (i != 0)
                return i;
            i = Boolean.compare(!o1.isDefaultRoute(), !o2.isDefaultRoute());
            if (i != 0)
                return i;
            return SortIP4First.comparePrivate(o1.getDestination().getAddress(), o2.getDestination().getAddress());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_info, container, false);

        final TextView textView = (TextView) root.findViewById(R.id.section_label);

        TelephonyManager tm = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
        tm.listen(l, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);

        refresh = new Runnable() {
            @Override
            public void run() {
                StringBuilder tt = new StringBuilder();

                ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                ArrayList<Network> nn = new ArrayList<>(Arrays.asList(cm.getAllNetworks()));
                Collections.sort(nn, new NetworkActiveSort(cm));
                StringBuilder w;
                for (Network n : nn) {
                    NetworkInfo info = cm.getNetworkInfo(n);
                    switch (info.getType()) {
                        case ConnectivityManager.TYPE_WIFI:
                            w = formatWifi(InfoFragment.this, new WifiInfo(getContext(), n));
                            if (w != null)
                                tt.append(w);
                            break;
                        case ConnectivityManager.TYPE_BLUETOOTH:
                            w = formatNet(getContext(), "Bluetooth", n);
                            if (w != null)
                                tt.append(w);
                            break;
                        case ConnectivityManager.TYPE_ETHERNET:
                            w = formatNet(getContext(), "Ethernet", n);
                            if (w != null)
                                tt.append(w);
                            break;
                        case ConnectivityManager.TYPE_MOBILE:
                            w = formatMobile(getContext(), signal, asu, n);
                            if (w != null)
                                tt.append(w);
                            break;
                        case ConnectivityManager.TYPE_WIMAX:
                            w = formatNet(getContext(), "WiMAX", n);
                            if (w != null)
                                tt.append(w);
                            break;
                        default:
                            w = formatNet(getContext(), "Network", n);
                            if (w != null)
                                tt.append(w);
                            break;
                    }
                }

                TreeSet<String> dd = new TreeSet<>(); // default interface names

                StringBuilder dns = formatDns(getContext());
                if (dns.length() != 0) {
                    tt.append(dns);
                    tt.append(NL);
                }

                StringBuilder r = formatRoutes(getContext(), dd);

                tt.append(formatNet(dd));

                if (r != null)
                    tt.append(r);

                textView.setText(Html.fromHtml(tt.toString()));
            }
        };

        FloatingActionButton fab = (FloatingActionButton) root.findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refresh.run();
            }
        });

        refresh.run();

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        TelephonyManager tm = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
        tm.listen(l, PhoneStateListener.LISTEN_NONE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        refresh.run();
    }
}
