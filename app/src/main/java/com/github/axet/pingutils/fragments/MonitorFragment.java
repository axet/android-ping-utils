package com.github.axet.pingutils.fragments;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.RouteInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.axet.androidlibrary.app.Ping;
import com.github.axet.androidlibrary.preferences.OptimizationPreferenceCompat;
import com.github.axet.androidlibrary.services.StorageProvider;
import com.github.axet.androidlibrary.widgets.OpenChoicer;
import com.github.axet.androidlibrary.widgets.OpenFileDialog;
import com.github.axet.pingutils.R;
import com.github.axet.pingutils.activities.MainActivity;
import com.github.axet.pingutils.widgets.AutoDetectAdapter;
import com.github.axet.pingutils.widgets.StatusTabView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MonitorFragment extends Fragment {
    public static String TAG = MonitorFragment.class.getSimpleName();

    public static int POLL = 5 * 1000; // regular polling time
    public static int FORCE_POLL = 30 * 1000; // ping gives no response for >30 seconds
    public static int HOST_SLOW_MS = 100; // yellow circle
    public static int HOST_ORANGE_MS = 1000; // yellow circle
    public static int SELECTED = 0x99999999;
    public static int MAXPINGS = 3; // ping history count

    public static String PREFS = "pref_hosts";
    public static String MS = "ms";

    public static final int RESULT_CODE = 1;
    private static final String ARG_SECTION_NUMBER = "section_number";

    public static String NL = "\n";

    Handler handler = new Handler();
    OpenChoicer c;
    HostsAdapter hosts;
    RecyclerView list;
    AlertDialog auto;
    Button auto_import;
    Runnable working = new Runnable() {
        @Override
        public void run() {
            start();
        }
    };

    public static MonitorFragment newInstance() {
        MonitorFragment fragment = new MonitorFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, 1);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static boolean isRoutable(Context context, String host) { // TODO 'ip route get 1'
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean ipv6 = host.contains(":");
        LinkProperties linkProperties = cm.getLinkProperties(cm.getActiveNetwork());
        if (linkProperties != null) {
            for (RouteInfo routeInfo : linkProperties.getRoutes()) {
                if (routeInfo.isDefaultRoute()) {
                    InetAddress a = routeInfo.getGateway();
                    if (ipv6) {
                        if (a instanceof Inet6Address)
                            return true;
                    } else {
                        if (a instanceof Inet4Address)
                            return true;
                    }
                }
            }
        }
        if (!ipv6) {
            WifiManager wm = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo info = wm.getConnectionInfo();
            if (info.getIpAddress() != 0)
                return true;
        }
        return false;
    }

    public static class Host implements Comparable<Host> {
        public String host; // ping host name
        public String description;
        public Thread thread;
        public int icon = R.drawable.circle_idle; // icon status
        public long last;
        public String ip; // last know resolved ip address
        ArrayList<Long> stats = new ArrayList<>(); // 0 == fail / unreachable

        public Host(String host) {
            this.host = host;
        }

        public Host(String host, String description) {
            this.host = host;
            this.description = description;
        }

        public Host(JSONObject o) {
            load(o);
        }

        public JSONObject save() {
            try {
                JSONObject o = new JSONObject();
                o.put("host", host);
                if (description != null)
                    o.put("description", description);
                return o;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        public void load(JSONObject j) {
            try {
                host = j.getString("host");
                description = j.optString("description");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        public String getHost() {
            if (ip == null || ip.isEmpty())
                return host;
            else
                return ip;
        }

        public String getDescription() {
            if (description == null || description.equals(host)) {
                if (ip != null && !ip.isEmpty() && !ip.equals(host))
                    return host;
                return null;
            }
            if (description.isEmpty() && ip != null && !ip.isEmpty() && !ip.equals(host))
                return host;
            else
                return description;
        }

        public boolean fail() {
            if (stats.size() == 0)
                return false;
            long ms = 0;
            for (Long p : stats)
                ms += p;
            return ms == 0;
        }

        public long getAverageMs() {
            long ms = 0;
            for (Long p : stats)
                ms += p;
            return ms / stats.size();
        }

        public void add(String ip, long ms) {
            if (ip != null && !ip.isEmpty())
                this.ip = ip;
            stats.add(ms);
            while (stats.size() > MAXPINGS)
                stats.remove(0);
        }

        public void updateIcon() {
            if (stats.size() == 0) {
                icon = R.drawable.circle_idle;
            } else if (fail()) {
                icon = R.drawable.circle_red;
            } else {
                if (getAverageMs() > HOST_ORANGE_MS)
                    icon = R.drawable.circle_orange;
                else if (getAverageMs() > HOST_SLOW_MS)
                    icon = R.drawable.circle_yellow;
                else
                    icon = R.drawable.circle_green;
            }
        }

        public String getIP() {
            if (ip != null)
                return ip;
            return host;
        }

        @Override
        public String toString() {
            return host + " (" + ip + ") " + description;
        }

        @Override
        public int compareTo(@NonNull Host o) {
            return host.compareTo(o.host);
        }
    }

    public static class HostHolder extends RecyclerView.ViewHolder {
        public ImageView status;
        public TextView ms;
        public TextView host;
        public TextView description;
        public Host h;

        public HostHolder(View itemView) {
            super(itemView);
            status = (ImageView) itemView.findViewById(R.id.status);
            ms = (TextView) itemView.findViewById(R.id.statustext);
            host = (TextView) itemView.findViewById(R.id.host);
            description = (TextView) itemView.findViewById(R.id.description);
        }

        public void update(Host h) {
            this.h = h;
            status.setImageResource(h.icon);
            if (h.stats.size() != 0 && !h.fail())
                ms.setText(h.getAverageMs() + MS);
            else
                ms.setText("");
            host.setText(h.getHost());
            description.setText(h.getDescription());
        }
    }

    public static class HostsAdapter extends RecyclerView.Adapter<HostHolder> {
        ArrayList<Host> list = new ArrayList<>();

        PopupMenu popup;

        public static Pattern HOST = Pattern.compile("([^ ]+)[ ](.*)");

        @Override
        public HostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View v = inflater.inflate(R.layout.host_item, parent, false);
            HostHolder h = new HostHolder(v);
            v.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return HostsAdapter.this.onLongClick(h);
                }
            });
            return h;
        }

        @Override
        public void onBindViewHolder(HostHolder holder, int position) {
            holder.update(list.get(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public void loadFile(InputStream is) {
            list.clear();
            try {
                BufferedReader buffer = new BufferedReader(new InputStreamReader(is));
                String section = null;
                while (buffer.ready()) {
                    String str = buffer.readLine().trim();
                    if (str.isEmpty())
                        continue;
                    if (str.startsWith("#"))
                        continue;
                    if (str.startsWith("!")) {
                        if (!list.isEmpty())
                            break;
                        else
                            section = str;
                        continue;
                    }
                    Matcher matcher = HOST.matcher(str);
                    if (matcher.find()) {
                        String host = matcher.group(1);
                        String name = matcher.group(2);
                        list.add(new Host(host, name));
                    } else {
                        list.add(new Host(str));
                    }
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            notifyDataSetChanged();
        }

        public boolean onLongClick(HostHolder h) {
            return false;
        }

        public String saveFile() {
            StringBuilder sb = new StringBuilder();
            sb.append("# Exported hosts");
            sb.append(NL);
            sb.append(NL);
            sb.append("!exported list");
            sb.append(NL);
            for (Host h : list) {
                sb.append(h.host);
                sb.append(" ");
                sb.append(h.description);
                sb.append(NL);
            }
            return sb.toString();
        }

        public void load(String str) {
            list.clear();
            try {
                JSONArray a = new JSONArray(str);
                for (int i = 0; i < a.length(); i++) {
                    Host h = new Host(a.getJSONObject(i));
                    list.add(h);
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        public String save() {
            JSONArray a = new JSONArray();
            for (Host h : list)
                a.put(h.save());
            return a.toString();
        }

        public void update(Host h) {
            if (popup != null)
                return;
            notifyItemChanged(list.indexOf(h));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    void load() {
        hosts = new HostsAdapter() {
            @Override
            public boolean onLongClick(HostHolder h) {
                View item = h.itemView;
                Drawable old = item.getBackground();
                item.setBackgroundColor(SELECTED);
                popup = new PopupMenu(h.itemView.getContext(), item);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_copy:
                                ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                                clipboard.setPrimaryClip(ClipData.newPlainText("host", h.h.host));
                                Toast.makeText(getContext(), "Host name copied", Toast.LENGTH_SHORT).show();
                                break;
                            case R.id.action_edit:
                                editDialog(h.h);
                                break;
                            case R.id.action_delete:
                                deleteDialog(h.h);
                                break;
                            case R.id.action_move_up:
                                int u = list.indexOf(h.h);
                                if (u > 0) {
                                    list.set(u - 1, list.set(u, list.get(u - 1)));
                                    notifyItemMoved(u, u - 1);
                                    MonitorFragment.this.save();
                                }
                                break;
                            case R.id.action_move_down:
                                int d = list.indexOf(h.h);
                                if (d < list.size() - 1) {
                                    list.set(d + 1, list.set(d, list.get(d + 1)));
                                    notifyItemMoved(d, d + 1);
                                    MonitorFragment.this.save();
                                }
                                break;
                        }
                        return true;
                    }
                });
                popup.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu menu) {
                        item.setBackground(old);
                        popup = null;
                    }
                });
                popup.inflate(R.menu.edit_menu);
                popup.show();
                return super.onLongClick(h);
            }
        };
        try {
            SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(getContext());
            String str = shared.getString(PREFS, "");
            hosts.load(str);
        } catch (RuntimeException e) {
            Log.e(TAG, "load", e);
        }
    }

    void save() {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor e = shared.edit();
        e.putString(PREFS, hosts.save());
        e.commit();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_monitor, container, false);

        load();

        list = (RecyclerView) root.findViewById(R.id.list);
        list.setLayoutManager(new LinearLayoutManager(getContext()));
        list.setAdapter(hosts);

        FloatingActionButton auto = (FloatingActionButton) root.findViewById(R.id.auto);
        FloatingActionButton add = (FloatingActionButton) root.findViewById(R.id.add);
        FloatingActionButton imp = (FloatingActionButton) root.findViewById(R.id.imports);
        FloatingActionButton exp = (FloatingActionButton) root.findViewById(R.id.exports);

        auto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder b = new AlertDialog.Builder(getContext());
                b = new AlertDialog.Builder(getContext());
                final FrameLayout f = (FrameLayout) LayoutInflater.from(getContext()).inflate(R.layout.importlist, null);
                ProgressBar idle = (ProgressBar) f.findViewById(R.id.idle);
                RecyclerView r = (RecyclerView) f.findViewById(R.id.list);
                idle.setVisibility(View.VISIBLE);
                r.setVisibility(View.GONE);
                r.setLayoutManager(new LinearLayoutManager(getContext()));
                final AutoDetectAdapter a = new AutoDetectAdapter(getContext());
                a.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                    @Override
                    public void onChanged() {
                        if (a.getItemCount() > 0) {
                            idle.setVisibility(View.GONE);
                            r.setVisibility(View.VISIBLE);
                            a.unregisterAdapterDataObserver(this);
                            MonitorFragment.this.auto_import.setEnabled(true);
                        }
                    }
                });
                r.setAdapter(a);
                b.setView(f);
                b.setTitle(R.string.auto_configurator);
                b.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ;
                    }
                });
                b.setPositiveButton(R.string.dialog_import, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        hosts.list.clear();
                        hosts.list.addAll(a.list);
                        hosts.notifyDataSetChanged();
                        save();
                    }
                });
                b.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        a.close();
                    }
                });
                MonitorFragment.this.auto = b.show();
                auto_import = MonitorFragment.this.auto.getButton(AlertDialog.BUTTON_POSITIVE);
                auto_import.setEnabled(false);
            }
        });

        imp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                c = new OpenChoicer(OpenFileDialog.DIALOG_TYPE.FILE_DIALOG, true) {
                    @Override
                    public void onResult(Uri uri) {
                        try {
                            ContentResolver r = context.getContentResolver();
                            InputStream is = r.openInputStream(uri);
                            hosts.loadFile(is);
                            save();
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    }
                };
                c.setStorageAccessFramework(MonitorFragment.this, RESULT_CODE);
                c.show(null);
            }
        });
        exp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = hosts.saveFile();
                String name = "ping-home.txt";
                Intent share = StorageProvider.shareIntent(getContext(), name, str);
                if (!OptimizationPreferenceCompat.startActivity(getContext(), share))
                    Toast.makeText(getContext(), "Unsupported", Toast.LENGTH_SHORT).show();
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDialog();
            }
        });

        start();

        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case RESULT_CODE:
                c.onActivityResult(resultCode, data);
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, "onDestoryView");
        stop();
        hosts = null;
    }

    public void start() {
        Log.d(TAG, "start");
        handler.removeCallbacks(working);
        handler.postDelayed(working, POLL);
        long now = System.currentTimeMillis();
        int status = R.drawable.circle_idle;
        long ms = 0;
        int mscount = 0;
        for (Host h : hosts.list) {
            switch (h.icon) {
                case R.drawable.circle_red:
                    status = R.drawable.circle_red;
                    break;
                case R.drawable.circle_orange:
                    if (status != R.drawable.circle_red)
                        status = R.drawable.circle_orange;
                    break;
                case R.drawable.circle_yellow:
                    if (status != R.drawable.circle_red && status != R.drawable.circle_orange)
                        status = R.drawable.circle_yellow;
                    break;
                case R.drawable.circle_green:
                    if (status == R.drawable.circle_idle)
                        status = R.drawable.circle_green;
                    break;
            }
            if (h.stats.size() != 0 && !h.fail()) {
                mscount++;
                ms += h.getAverageMs();
            }
            if (h.thread != null) {
                if (h.last + FORCE_POLL > now)
                    continue;
                h.add(null, FORCE_POLL);
                h.updateIcon();
                h.thread.interrupt();
            }
            if (h.last + POLL < now) {
                h.last = System.currentTimeMillis();
                h.thread = new Thread("Ping Host thread") {
                    @Override
                    public void run() {
                        try {
                            if (!isRoutable(getContext(), h.host)) { // ignore ipv6 hosts, if wifi has no ipv6 (not optimal)
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (hosts == null)
                                            return;
                                        h.updateIcon();
                                        hosts.update(h);
                                    }
                                });
                                return;
                            }
                            Ping ping = PingFragment.newPing(getContext(), h.host, PingFragment.detect6(h.host, false));
                            ping.ping();
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (hosts == null)
                                        return;
                                    if (ping.fail())
                                        h.add(ping.ip, 0);
                                    else
                                        h.add(ping.ip, ping.ms);
                                    h.updateIcon();
                                    hosts.update(h);
                                }
                            });
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (hosts == null)
                                        return;
                                    h.add(null, 0);
                                    h.updateIcon();
                                    hosts.update(h);
                                }
                            });
                            Log.e(TAG, "ping thread", e);
                        } finally {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    h.thread = null;
                                }
                            });
                        }
                    }
                };
                h.thread.start();
            }
        }
        String mst = null;
        if (mscount != 0 && status != R.drawable.circle_idle)
            mst = (ms / mscount) + MS;
        status(status, mst);
    }

    public void stop() {
        Log.d(TAG, "stop");
        handler.removeCallbacks(working);
        status(R.drawable.circle_idle, null);
        if (auto != null) {
            auto.dismiss();
            auto = null;
        }
    }

    void status(int i, String ms) {
        TabLayout tabs = ((MainActivity) getActivity()).tabs;
        TabLayout.Tab t = tabs.getTabAt(MainActivity.STATUS_TAB_INDEX);
        StatusTabView status = (StatusTabView) t.getCustomView();
        status.loadImage(i);
        status.setText(ms);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (hosts != null)
            start();
    }

    @Override
    public void onPause() {
        super.onPause();
        stop();
    }

    void addDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add Host");
        final View view = LayoutInflater.from(getContext()).inflate(R.layout.host_edit_item, null);
        builder.setView(view);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                TextView host = (TextView) view.findViewById(R.id.host);
                TextView desc = (TextView) view.findViewById(R.id.description);
                String h = host.getText().toString().trim();
                String d = desc.getText().toString().trim();
                if (h.isEmpty())
                    return;
                hosts.list.add(new Host(h, d));
                save();
            }
        });
        builder.create().show();
    }

    void editDialog(Host h) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Edit Host");
        final View view = LayoutInflater.from(getContext()).inflate(R.layout.host_edit_item, null);
        TextView host = (TextView) view.findViewById(R.id.host);
        TextView desc = (TextView) view.findViewById(R.id.description);
        host.setText(h.host);
        desc.setText(h.description);
        builder.setView(view);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String h1 = host.getText().toString().trim();
                String d1 = desc.getText().toString().trim();
                if (h1.isEmpty())
                    return;
                h.host = h1;
                h.description = d1;
                hosts.notifyItemChanged(hosts.list.indexOf(h));
                save();
            }
        });
        builder.create().show();
    }

    void deleteDialog(Host h) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Delete Host");
        builder.setMessage("Are you sure?");
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int p = hosts.list.indexOf(h);
                hosts.list.remove(h);
                hosts.notifyItemRemoved(p);
                save();
            }
        });
        builder.create().show();
    }
}