package com.github.axet.pingutils.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.github.axet.androidlibrary.app.Network;
import com.github.axet.androidlibrary.app.Ping;
import com.github.axet.androidlibrary.app.PingExt;
import com.github.axet.androidlibrary.widgets.OpenFileDialog;
import com.github.axet.pingutils.R;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class RouteFragment extends Fragment {
    public static String TAG = RouteFragment.class.getSimpleName();

    private static final String ARG_SECTION_NUMBER = "section_number";

    Handler handler = new Handler();
    Thread thread;
    Snackbar sb;

    public static RouteFragment newInstance() {
        RouteFragment fragment = new RouteFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, 1);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static Ping newPing(Context context, String host, boolean ipv6, int i) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        if (shared.getBoolean("ping", false))
            return new Ping(host, ipv6, i);
        else
            return new PingExt(host, ipv6, i);
    }

    public static void traceroute(Context context, String host, boolean ipv6, int count, StringBuilder out, Handler handler, Runnable run) {
        try {
            InetAddress address = Ping.getHostByName(ipv6, host);
            if (address == null) {
                out.append("Unknown host: ");
                out.append(host);
                return;
            }
            out.append(String.format("traceroute to %s (%s), 30 hops max", host, address.getHostAddress()));
        } catch (UnknownHostException e) {
            Log.w(TAG, e);
        }
        out.append("\n");
        handler.post(run);
        for (int i = 1; i < count; i++) {
            Ping p1 = newPing(context, host, ipv6, i);
            p1.ping();
            out.append(String.format("%d  ", i));
            if (p1.fail()) {
                out.append("* ");
            } else {
                if (p1.hoop != null)
                    out.append(String.format("%s %s  ", p1.hoop, p1.hoopIp));
                else
                    out.append(String.format("%s  ", p1.hoopIp));
                out.append(String.format("%d ms ", p1.ms));
            }
            handler.post(run);
            Ping p2 = newPing(context, host, ipv6, i);
            p2.ping();
            if (p2.fail())
                out.append("* ");
            else
                out.append(String.format("%d ms ", p2.ms));
            handler.post(run);
            Ping p3 = newPing(context, host, ipv6, i);
            p3.ping();
            if (p3.fail())
                out.append("* ");
            else
                out.append(String.format("%d ms", p3.ms));
            out.append("\n");
            handler.post(run);
            if (p1.ttlex || p2.ttlex || p3.ttlex)
                continue;
            if (!p1.ttlex && !p2.ttlex && !p3.ttlex && (p1.ttlr != 0 || p2.ttlr != 0 || p3.ttlr != 0))
                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_route, container, false);

        FloatingActionButton fab = (FloatingActionButton) root.findViewById(R.id.fab);

        final EditText edit = (EditText) root.findViewById(R.id.ping_edit);
        final TextView output = (TextView) root.findViewById(R.id.output);
        final CheckBox ipv6 = (CheckBox) root.findViewById(R.id.ipv6);

        if (edit.getText().toString().isEmpty())
            edit.setText(Network.getGatewayIP(getContext()));

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenFileDialog.hideKeyboard(getContext(), edit);
                if (thread != null)
                    thread.interrupt();
                thread = new Thread() {
                    @Override
                    public void run() {
                        final StringBuilder out = new StringBuilder();
                        Runnable run = new Runnable() {
                            @Override
                            public void run() {
                                output.setText(out.toString());
                            }
                        };
                        Runnable done = new Runnable() {
                            @Override
                            public void run() {
                                handler.post(run);
                                sb.dismiss();
                            }
                        };
                        String host = edit.getText().toString().trim();
                        try {
                            traceroute(getContext(), host, ipv6.isChecked(), 30, out, handler, run);
                        } catch (RuntimeException e) {
                            out.append(e);
                        }
                        done.run();
                    }
                };
                sb = Snackbar.make(view, "Quering", Snackbar.LENGTH_LONG)
                        .setAction("Action", null)
                        .setDuration(Snackbar.LENGTH_INDEFINITE);
                sb.show();
                thread.start();
            }
        });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}