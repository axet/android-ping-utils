package com.github.axet.pingutils.widgets;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.github.axet.androidlibrary.app.Ping;
import com.github.axet.androidlibrary.widgets.ThemeUtils;
import com.github.axet.pingutils.app.DnsServersDetector;
import com.github.axet.pingutils.fragments.DigFragment;
import com.github.axet.pingutils.fragments.InfoFragment;
import com.github.axet.pingutils.fragments.MonitorFragment;
import com.github.axet.pingutils.fragments.PingFragment;

import org.apache.commons.net.util.SubnetUtils;

import java.io.IOException;
import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

public class AutoDetectAdapter extends MonitorFragment.HostsAdapter {
    public static String TAG = AutoDetectAdapter.class.getSimpleName();

    public static int POLL = 2000;
    public static int COUNT = 10;
    public static int HOPS_MIN = 10;
    public static int COLOR_LOCAL = 0x22000000;
    public static int COLOR_KNOWN = 0x2200ff00;
    public static int COLOR_IPV6 = 0x22ffff0f;
    public static int COLOR_IPV4 = 0x22f1c40f;

    public ArrayList<MonitorFragment.Host> list = new ArrayList<>();

    public TreeSet<MonitorFragment.Host> localFound = new TreeSet<>(); // detected local hosts
    public ArrayList<MonitorFragment.Host> localPing = new ArrayList<>(); // detecting local hosts
    public ArrayList<SubnetUtils.SubnetInfo> local = new ArrayList<>(); // pending local subnets
    public ArrayList<Hop> hopsFound = new ArrayList<>(); // found hops IP's
    public ArrayList<Hop> hopsPing = new ArrayList<>(); // pinged hops IP's
    public ArrayList<Hop> hops = new ArrayList<>(); // pending hops IP's
    public int hopsIndex = 0;
    public TreeSet<MonitorFragment.Host> knowFound = new TreeSet<>(); // detected know hosts
    public ArrayList<MonitorFragment.Host> knowPing = new ArrayList<>(); // know static IP's
    public ArrayList<MonitorFragment.Host> know = new ArrayList<>(); // pending know static IP's

    Context context;
    Handler handler = new Handler();

    Runnable poll = new Runnable() {
        @Override
        public void run() {
            poll();
        }
    };

    public static String GOOGLE_DNS4 = "8.8.8.8";
    public static String GOOGLE_DNS4_2 = "8.8.4.4";
    public static String GOOGLE_DNS6 = "2001:4860:4860::8888";
    public static String GOOGLE_DNS6_2 = "2001:4860:4860::8844";
    public static String YANDEX_DNS4 = "77.88.8.8";
    public static String YANDEX_DNS6 = "2a02:6b8::feed:ff";
    public static String DNSEU_DNS4 = "193.110.81.0"; // dns0.eu
    public static String DNSEU_DNS6 = "2a0f:fc81::"; // dns0.eu
    public static String DNSPOD_DNS4 = "119.29.29.29"; // dns.icoa.cn
    public static String DNSPOD_DNS6 = "2402:4e00::"; // dns.icoa.cn

    public static boolean detect6(String host, boolean ipv6) { // true - ipv6, false - ipv4
        if (host.contains(":"))
            return true;
        if (PingFragment.IPv4.matcher(host).matches())
            return false;
        return ipv6;
    }

    public static <T> Collection<T> filter(Collection<T> list, Filter<T> f) {
        ArrayList<T> nn = new ArrayList<>();
        for (T l : list) {
            if (f.filter(l))
                nn.add(l);
        }
        return nn;
    }

    public static String reverseLookupDns(String h) throws IOException {
        InetAddress ia = InetAddress.getByName(h);
        return ia.getCanonicalHostName();
    }

    public void reverseLookupDns(Context context, MonitorFragment.Host h) {
        if (h.ip == null) {
            DnsServersDetector dns = new DnsServersDetector(context);
            DnsServersDetector.Pair[] ss = dns.getServers();
            String[] kk = DnsServersDetector.toNames(ss);
            for (String k : kk) {
                try {
                    String a = DigFragment.reverseLookupDns(k, h.host);
                    if (a != null && !a.equals(h.host)) {
                        h.ip = h.host;
                        h.host = a;
                        return;
                    }
                } catch (IOException e) {
                    Log.w(TAG, e);
                }
            }
            try {
                String a = reverseLookupDns(h.host);
                if (a != null && !a.equals(h.host)) {
                    h.ip = h.host;
                    h.host = a;
                }
            } catch (IOException e1) {
                Log.w(TAG, e1);
            }
        }
    }

    public static BigInteger parseIp(String addr) {
        try {
            InetAddress a = InetAddress.getByName(addr);
            return new BigInteger(a.getAddress());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public interface Filter<T> {
        boolean filter(T t); // true to add
    }

    public static class Hop extends MonitorFragment.Host {
        public int hop; // hop index
        public int ttl; // ttl returned
        public Hop old;
        public boolean ipv6;

        public Hop(String host, String des, int ttl) {
            super(host, des);
            hop = ttl;
        }

        public Hop(Hop old) {
            super(old.getIP());
            hop = old.hop;
            this.old = old;
        }

        public boolean clean() { // can we queue host again if ttl reply failed?
            if (old == null)
                return true;
            return !getIP().equals(old.getIP());
        }
    }

    public static class SortHop implements Comparator<Hop> {
        @Override
        public int compare(Hop o1, Hop o2) {
            int i = Boolean.compare(o1.ipv6, o2.ipv6);
            if (i != 0)
                return i;
            return Integer.compare(o1.hop, o2.hop);
        }
    }

    public static class SortIP implements Comparator<MonitorFragment.Host> {
        @Override
        public int compare(MonitorFragment.Host o1, MonitorFragment.Host o2) {
            try {
                return parseIp(o1.getIP()).compareTo(parseIp(o2.getIP()));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static class SortKnowOrder implements Comparator<MonitorFragment.Host> {
        ArrayList<MonitorFragment.Host> list;

        public SortKnowOrder(ArrayList<MonitorFragment.Host> l) {
            list = l;
        }

        int indexOf(MonitorFragment.Host h) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).host.equals(h.host))
                    return i;
            }
            return -1;
        }

        @Override
        public int compare(MonitorFragment.Host o1, MonitorFragment.Host o2) {
            return Integer.compare(indexOf(o1), indexOf(o2));
        }
    }

    public AutoDetectAdapter(Context context) {
        this.context = context;
        detectKnow();
        detectLocal();
        detectHop(2);
        poll();
    }

    public void detectHop(int ttl) {
        hopsIndex = ttl;
        hops.clear();
        hops.add(new Hop(YANDEX_DNS4, "yandex dns4", ttl)); // ru
        hops.add(new Hop(YANDEX_DNS6, "yandex dns6", ttl)); // ru
        hops.add(new Hop(DNSEU_DNS4, "eu dns4", ttl)); // eu
        hops.add(new Hop(DNSEU_DNS6, "eu dns6", ttl)); // eu
        hops.add(new Hop(GOOGLE_DNS4, "google dns4", ttl)); // us
        hops.add(new Hop(GOOGLE_DNS6, "google dns6", ttl)); // us
        hops.add(new Hop(DNSPOD_DNS4, "china dns4", ttl)); // cn
        hops.add(new Hop(DNSPOD_DNS6, "china dns6", ttl)); // cn
    }

    public void detectKnow() {
        know.clear();
        know.add(new MonitorFragment.Host(GOOGLE_DNS4, "google dns4"));
        know.add(new MonitorFragment.Host(GOOGLE_DNS6, "google dns6"));
        know.add(new MonitorFragment.Host(YANDEX_DNS4, "yandex dns4"));
        know.add(new MonitorFragment.Host(YANDEX_DNS6, "yandex dns6"));
    }

    public void detectLocal() {
        local.clear();
        try {
            ArrayList<NetworkInterface> nn = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface ni : nn) {
                List<InterfaceAddress> aa = ni.getInterfaceAddresses();
                for (InterfaceAddress i : aa) {
                    InetAddress a = i.getAddress();
                    if (a instanceof Inet4Address) {
                        if (InfoFragment.isIPPrivate(a) && !a.isLoopbackAddress()) {
                            SubnetUtils.SubnetInfo info = new SubnetUtils(i.getAddress().getHostAddress() + "/" + i.getNetworkPrefixLength()).getInfo();
                            info = new SubnetUtils(info.getLowAddress(), info.getNetmask()).getInfo();
                            local.add(info);
                        }
                    }
                }
            }
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
    }

    public void poll() {
        nextLocals();
        nextKnown();
        nextHop();
        list.clear();
        ArrayList<MonitorFragment.Host> ll = new ArrayList<>(localFound);
        Collections.sort(ll, new SortIP());
        list.addAll(ll);
        list.addAll(hopsFound);
        ArrayList<MonitorFragment.Host> kk = new ArrayList<>(knowFound);
        Collections.sort(kk, new SortKnowOrder(know));
        list.addAll(kk);
        notifyDataSetChanged();
        handler.postDelayed(poll, POLL);
    }

    public void nextHop() {
        for (Hop h : hops) {
            if (h.thread != null)
                continue;
            h.thread = new Thread("hop thread " + h.host) {
                @Override
                public void run() {
                    Ping ping = PingFragment.newPing(context, h.host, PingFragment.detect6(h.host, false), h.hop);
                    try {
                        ping.ping();
                        if (ping.fail())
                            h.add(ping.ip, 0);
                        else
                            h.add(ping.ip, ping.ms);
                        if (ping.hoopIp == null)
                            h.host = ping.host;
                        else
                            h.host = ping.hoopIp;
                        h.ip = null;
                        reverseLookupDns(context, h);
                        h.ttl = ping.ttlr;
                        h.ipv6 = ping.ipv6;
                        if (h.ip != null) {
                            String d = "";
                            if (!h.host.equals(h.ip)) {
                                d = h.host + " ";
                                h.host = h.ip; // keep ip, ignore host
                            }
                            h.description = d + "(hop" + h.hop + ")";
                        } else {
                            h.description = "(hop" + h.hop + ")";
                        }
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                hopsPing.add(h);
                            }
                        });
                    } catch (Exception e) {
                        Log.w(TAG, e);
                    } finally {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                h.updateIcon();
                                hops.remove(h);
                                h.thread = null;
                            }
                        });
                    }
                }
            };
            h.thread.start();
        }
        if (hops.isEmpty()) {
            if (!hopsPing.isEmpty()) {
                boolean b = hopsFilter(false);
                b |= hopsFilter(true);
                hopsPing.clear();
                if (b || hopsIndex < HOPS_MIN && hops.isEmpty())
                    detectHop(hopsIndex + 1);
            } else {
                detectHop(2);
            }
        }
    }

    public boolean hopsFilter(boolean ipv6) {
        Collection<Hop> hopsPing = filter(AutoDetectAdapter.this.hopsPing, new Filter<Hop>() {
            @Override
            public boolean filter(Hop t) {
                return !ipv6 && !t.ipv6 || ipv6 && t.ipv6;
            }
        });
        if (hopsPing.isEmpty())
            return false;
        boolean ttl = false;
        boolean clean = false; // can we queue same host again?
        for (Hop h : hopsPing) {
            if (h.clean())
                clean = true;
            if (h.ttl != 0)
                ttl = true;
        }
        if (!ttl && clean) {
            for (Hop h : hopsPing)
                hops.add(new Hop(h));
        } else {
            TreeMap<Integer, Hop> mm = new TreeMap<>();
            for (Hop h : hopsPing) {
                int count = 0;
                for (Hop k : hopsPing) {
                    if (k.host.equals(h.host))
                        count++;
                }
                mm.put(count, h);
            }
            Map.Entry<Integer, Hop> last = mm.lastEntry();
            int count = last.getKey();
            Hop h = last.getValue();
            if (count > 1) {
                for (int k = 0; k < hopsFound.size(); k++) {
                    Hop hh = hopsFound.get(k);
                    if (hh.hop == h.hop && detect6(hh.getIP(), ipv6) == ipv6) {
                        hopsFound.remove(k);
                        break;
                    }
                }
                Log.d(TAG, "adding hop: " + h + " " + count);
                hopsFound.add(h);
                Collections.sort(hopsFound, new SortHop());
                return true;
            }
        }
        return false;
    }

    public void nextKnown() {
        for (int i = 0; i < COUNT && !know.isEmpty(); i++) {
            MonitorFragment.Host h = know.remove(0);
            h.thread = new Thread("Ping known host " + h.host) {
                @Override
                public void run() {
                    Ping ping = PingFragment.newPing(context, h.host, PingFragment.detect6(h.host, false));
                    try {
                        ping.ping();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (ping.fail()) {
                                    h.add(ping.ip, 0);
                                } else {
                                    h.add(ping.ip, ping.ms);
                                    Log.d(TAG, "know found: " + h);
                                    knowFound.add(h);
                                }
                            }
                        });
                    } catch (Exception e) {
                        Log.w(TAG, e);
                    } finally {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                h.updateIcon();
                                knowPing.remove(h);
                                h.thread = null;
                            }
                        });
                    }
                }
            };
            h.thread.start();
            knowPing.add(h);
        }
        if (know.isEmpty())
            detectKnow();
    }

    public void nextLocals() {
        for (int i = 0; i < COUNT && !local.isEmpty() && localPing.size() < COUNT; i++) {
            SubnetUtils.SubnetInfo info = local.get(0);
            MonitorFragment.Host h = new MonitorFragment.Host(info.getAddress());
            SubnetUtils.SubnetInfo next = new SubnetUtils(info.getNextAddress(), info.getNetmask()).getInfo();
            if (info.isInRange(next.getAddress()))
                local.set(0, next);
            else
                local.remove(0);
            if (info.getBroadcastAddress().equals(info.getAddress()))
                continue;
            h.thread = new Thread("Ping locals thread host " + h.host) {
                @Override
                public void run() {
                    Ping ping = PingFragment.newPing(context, h.host, PingFragment.detect6(h.host, false));
                    try {
                        ping.ping();
                        if (!ping.fail())
                            reverseLookupDns(context, h);
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (ping.fail()) {
                                    h.add(ping.ip, 0);
                                } else {
                                    h.add(ping.ip, ping.ms);
                                    Log.d(TAG, "local found: " + h);
                                    localFound.add(h);
                                }
                            }
                        });
                    } catch (Exception e) {
                        Log.w(TAG, e);
                    } finally {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                h.updateIcon();
                                localPing.remove(h);
                                h.thread = null;
                            }
                        });
                    }
                }
            };
            h.thread.start();
            localPing.add(h);
        }
        if (local.isEmpty())
            detectLocal();
    }

    public void close() {
        handler.removeCallbacksAndMessages(null);
    }

    @Override
    public void onBindViewHolder(MonitorFragment.HostHolder holder, int position) {
        MonitorFragment.Host h = list.get(position);
        holder.update(h);
        if (localFound.contains(h)) {
            holder.itemView.setBackgroundColor(COLOR_LOCAL);
        } else if (knowFound.contains(h)) {
            holder.itemView.setBackgroundColor(COLOR_KNOWN);
        } else if (hopsFound.contains(h)) {
            Hop hh = (Hop) h;
            if (hh.ipv6)
                holder.itemView.setBackgroundColor(COLOR_IPV6);
            else
                holder.itemView.setBackgroundColor(COLOR_IPV4);
        } else {
            holder.itemView.setBackgroundColor(ThemeUtils.getThemeColor(context, android.R.attr.windowBackground));
        }
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ClipboardManager c = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                String str = h.getIP();
                c.setPrimaryClip(ClipData.newPlainText("ip", str));
                com.github.axet.androidlibrary.widgets.Toast.makeText(context, "Copied: " + str, com.github.axet.androidlibrary.widgets.Toast.LENGTH_LONG).show();
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
