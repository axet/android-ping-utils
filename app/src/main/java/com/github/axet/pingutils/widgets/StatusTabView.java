package com.github.axet.pingutils.widgets;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.axet.pingutils.R;

public class StatusTabView extends FrameLayout {
    public static final String DOTS = "◯";

    Drawable d;
    ColorStateList colors;
    AppCompatImageView image;
    TextView text;

    public static void update(TabLayout tab, int index) {
        StatusTabView v = new StatusTabView(tab.getContext(), tab.getTabTextColors());
        TabLayout.Tab t = tab.getTabAt(index);
        v.updateLayout(t);
    }

    public StatusTabView(Context context, ColorStateList colors) {
        super(context);
        this.colors = colors;
        image = new AppCompatImageView(context);
        FrameLayout.LayoutParams lp = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.gravity = Gravity.CENTER;
        addView(image, lp);
        text = new TextView(context);
        text.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 4);
        text.setTextColor(0xff000000);
        lp = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.gravity = Gravity.CENTER;
        addView(text, lp);
        loadImage(R.drawable.circle_idle);
    }

    public void loadImage(int i) {
        d = ContextCompat.getDrawable(getContext(), i);
        image.setImageDrawable(d);
        image.setColorFilter(colors.getDefaultColor());
    }

    public void setText(String ms) {
        text.setText(ms);
    }

    public void updateLayout(TabLayout.Tab tab) {
        tab.setCustomView(this);
        ViewParent p = getParent();
        if (p instanceof LinearLayout) { // TabView extends LinearLayout
            LinearLayout l = (LinearLayout) p;
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) l.getLayoutParams();
            if (lp != null) {
                lp.weight = 0;
                lp.width = LinearLayout.LayoutParams.WRAP_CONTENT;
                int left = l.getMeasuredHeight() / 2 - d.getIntrinsicWidth() / 2;
                int right = left;
                left -= l.getPaddingLeft();
                right -= l.getPaddingRight();
                if (left < 0)
                    left = 0;
                if (right < 0)
                    right = 0;
                setPadding(left, 0, right, 0);
            }
        }
    }
}
