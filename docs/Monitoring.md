# Monitoring

Understanding network issues can be complicated. Without network tools, it could be very complicated.

Localization network issues can be done using ping utilities by creating server ping list. Here is a recommendation of servers can be added to the ping list:

1) First make sure Wifi is working. Local IP is assigned and DNS assosialocated with the android network interface. All those questions can be answered using Ping Utils 'INFO' tab.

2) Second. Create local servers IP's from your local network. Do not forget booth protocols IPv4 and IPv6 servers. Those servers has to be responsible for local netowrk issues.

  * Default router/gateway local IPv4, IPv6 (for example: 192.168.1.1, fc00:10:10:5::1/64)
  * Default router/gateway external IPv4, IPv6 (172.158.15.1, 2001:17:27:184::2)
  * Wifi Bridges devices IP's / Access Points IP's (example: 192.168.1.5)
  * Local static / dynamic (by name) Printers / Servers IP's (example: 192.168.1.10, rpi)

3) Create your provider tracerouting tables.

Running 'traceroute google.com', 'traceroute yandex.ru' can be very informative.

Analyze traceroute output to different internet servers in different regions (Euroe, America, Russia) and see how traffic is managed, record basic routing, record same/different gateway of your provider. Monitoring network overtime, can also show different provider gateways of different global providers (for example when main provider gate is not working, temporary gate can be enabled).

Keep eye on TTL values. While monitoring provider routing servers, TTL goes down every hop. More TTL lost, mean more servers are doing routing tasks. And eventially it's become clear when IP packets leave provider servers when IP address stop repeating, usually takes 5-7 steps. You can ping every host, record TTL values, and find invisible servers. Record them all as (host ip, description hop1; host ip, description hop2). TTL usually can be 64 based value (linux) or 255 based values (windows).

4) Add global known IP's from different regions (like global DNS)

  * google.com IPv4, IPv6
  * google dns IP's
  * yandex.ru IPv4, IPv6
  * yandex dns IP's

## Examples

![example1](/docs/monitoring_example.png)